<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

<jsp:include page="/views/index.jsp" />

<h1>SUGESTÃO CADASTRADA!!!</h1>

<h1>Suas Reservas</h1>
	<div class="container">
			<table border="1">
				<tr>
					<th>ID</th>
					<th>Titulo</th>
					<th>Autor</th>
				</tr>
				<c:forEach items="${sugestoes}" var="sugestao">
					<tr id="pedido_${reserva.livro.id}">
						<td>${sugestao.id}</td>
						<td>${sugestao.titulo}</td>
						<td>${sugestao.autor}</td>
					</tr>
				</c:forEach>
			</table>
	</div>

</body>
</html>