<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="/views/index.jsp" />
	<h1>Confirmar pedido?</h1>
	<div class="container">
		<form action="confirmarPedido">
			<table border="1">
				<tr>
					<th>Titulo</th>
					<th>Quantidade</th>
				</tr>
				<c:forEach items="${pedidoReserva}" var="pedido">
					<tr id="pedido_${pedido.livro.id}">
						<td>${pedido.livro.nome}</td>
						<td>${pedido.quantidade}</td>
					</tr>
				</c:forEach>
			</table>
			<input type="submit" value="Confirmar Pedido!">
		</form>
	</div>
</body>
</html>