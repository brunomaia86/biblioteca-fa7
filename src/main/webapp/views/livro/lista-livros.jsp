<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="/views/index.jsp" />
	<div class="container">
			<table border="1">
				<tr>
					<th>Id</th>
					<th>Titulo</th>
					<th>Escritor</th>
					<th>Ano do Lançamento</th>
					<th>Classificação</th>
					<th>Quantidade Disponivel</th>
				</tr>
				<c:forEach items="${livros}" var="livro">
					<tr id="tarefa_${livro.id}">
						<td>${livro.id}</td>
						<td>${livro.nome}</td>
						<td>${livro.escritor}</td>
						<td>${livro.anoedicao}</td>
						<td>${livro.classificacao}</td>
						<td>${livro.quantidade}</td>
						<td><a href="mostrar?id=${livro.id}">Alterar</a></td>
						<td><a href="/biblioteca/reserva/mostrar?id=${livro.id}">Reservar!</a></td>
					</tr>
				</c:forEach>
			</table>

	</div>
</body>
</html>