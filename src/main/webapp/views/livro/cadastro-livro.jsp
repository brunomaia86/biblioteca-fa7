<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Cadastro de Livro</title>
</head>
<body>
	<div align="center">
		<h1>Cadastro de Livro</h1>
		<form action="cadastrar" method="post">
			Titulo: <input type="text" name="nome">
			Escritor: <input type="text" name="escritor">
			Ano: <input type="text" name="anoedicao">
			Quantidade: <input type="text" name="quantidade">
			<input type="submit" value="Cadastrar">
		</form>
	</div>

</body>
</html>