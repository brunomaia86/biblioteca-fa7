<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Biblioteca FA7</title>
<link rel="stylesheet"
	href="<c:url value="/assets/css/bootstrap.min.css" /> ">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2>Sistema Biblioteca</h2>
				<p><a href="/biblioteca/pessoa/form">CADASTRAR PESSOA</a></p>
				<p><a href="/biblioteca/livro/form">CADASTRAR LIVRO</a></p>
				<p><a href="/biblioteca/livro/listar">LISTAR LIVROS</a></p>
				<p><a href="/biblioteca/reserva/verificarPedido">VERIFICAR PEDIDO</a></p>
				<p><a href="/biblioteca/reserva/verificarReserva">VERIFIRAR RESERVA</a></p>
				<p><a href="/biblioteca/sugestao/form">SUGERIR NOVOS LIVROS</a></p>
			</div>
		</div>
		<br />
		<h5><a href="/biblioteca/logout" >LOGOUT</a></h5>
		<h1>------------------------------------------------------------</h1>
	</div>
</body>
</html>