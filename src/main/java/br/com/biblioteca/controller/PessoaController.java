package br.com.biblioteca.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.biblioteca.dao.PessoaDAO;
import br.com.biblioteca.model.Pessoa;

@Controller
@RequestMapping("/pessoa")
public class PessoaController {
	
	@Autowired
	private PessoaDAO pessoaDAO;
	

	@RequestMapping("/form")
	public String form() {
		return "pessoa/cadastro-pessoa";
	}
	
	@RequestMapping(value = "/cadastrar", method = RequestMethod.POST)
	public ModelAndView novaPessoa(@ModelAttribute Pessoa pessoa) {
		pessoaDAO.save(pessoa);
		ModelAndView model = new ModelAndView("pessoa/cadastro-sucesso");
		String msg = "SUCESSO CADASTRO DE PESSOA";
		model.addObject("msg", msg);
		return model;
	}
	
}
