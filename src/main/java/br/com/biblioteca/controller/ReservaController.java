package br.com.biblioteca.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.biblioteca.dao.LivroDAO;
import br.com.biblioteca.dao.ReservaDAO;
import br.com.biblioteca.model.Livro;
import br.com.biblioteca.model.Pessoa;
import br.com.biblioteca.model.ReservaSimples;

@Controller
@RequestMapping("/reserva")
public class ReservaController {

	@Autowired
	private LivroDAO livroDAO;

	@Autowired
	private ReservaDAO reservaDAO;

	@RequestMapping("/form")
	public String pedido() {

		return "mostrar";
	}

	@RequestMapping("/verificarPedido")
	public ModelAndView verificarPedido(HttpSession request) {
		ModelAndView mv;
		List<ReservaSimples> reservaPendente = (List<ReservaSimples>) request.getAttribute("pedidoReserva");
		mv = new ModelAndView("reserva/confirma-reserva");
		mv.addObject("pedidoReserva", reservaPendente);
		return mv; 
	}
	
	@RequestMapping("/verificarReserva")
	public ModelAndView verificarReserva() {
		List<ReservaSimples> reservas = reservaDAO.searchModels(DetachedCriteria.forClass(ReservaSimples.class));
			ModelAndView mv = new ModelAndView("reserva/verifica-reserva");
			mv.addObject("reservas", reservas);
			return mv;
	}

	@RequestMapping("/mostrar")
	public ModelAndView mostrarReserva(int id) {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Livro.class).add(Restrictions.idEq(id));
		Livro livro = livroDAO.searchModel(detachedCriteria);
		System.out.println(livro.getNome());
		ModelAndView mv = new ModelAndView("reserva/pedido-reserva");
		mv.addObject("livro", livro);
		return mv;
	}

	@RequestMapping("/pedido")
	public ModelAndView realizarPedido(Livro livro, int quantidade, HttpSession request) {
		ModelAndView mv = new ModelAndView("reserva/confirma-reserva");
		List<ReservaSimples> pedidoReserva = new ArrayList<>();
		Pessoa pessoaLogada = (Pessoa) request.getAttribute("usuLogado");
		System.out.println(pessoaLogada.getNome());
		if (request.getAttribute("pedidoReserva") == null) {
			pedidoReserva.add(new ReservaSimples(pessoaLogada, livro, quantidade));
			request.setAttribute("pedidoReserva", pedidoReserva);
			mv.addObject("pedidoReserva", pedidoReserva);
			return mv;
		}
		pedidoReserva = (List<ReservaSimples>) request.getAttribute("pedidoReserva");
		pedidoReserva.add(new ReservaSimples(pessoaLogada, livro, quantidade));
		mv.addObject("pedidoReserva", pedidoReserva);
		return mv;
	}

	@RequestMapping("/confirmarPedido")
	public String confirmaPedido(HttpSession request) {
		List<ReservaSimples> reservaPendente = (List<ReservaSimples>) request.getAttribute("pedidoReserva");
		for (ReservaSimples reservaSimples : reservaPendente) {
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Livro.class)
					.add(Restrictions.idEq(reservaSimples.getLivro().getId()));
			Livro livro = livroDAO.searchModel(detachedCriteria);
			if (reservaSimples.getQuantidade() > livro.getQuantidade()) {
				reservaSimples.setQuantidade(livro.getQuantidade());
				livro.setQuantidade(0);
				livroDAO.update(livro);
				reservaDAO.save(reservaSimples);
			} else {
				livro.setQuantidade(livro.getQuantidade() - reservaSimples.getLivro().getQuantidade());
				livroDAO.update(livro);
				reservaDAO.save(reservaSimples);
			}
		}
		request.removeAttribute("pedidoReserva");
		return "reserva/pedido-confirmado";
	}
	
	@RequestMapping("/apagarReserva")
	public ModelAndView apargarReserva (int idReserva) {
		System.out.println("entrou");
		System.out.println(idReserva);
		DetachedCriteria detachedCriteriaReserva = DetachedCriteria.forClass(ReservaSimples.class)
				.add(Restrictions.idEq(idReserva));
		ReservaSimples reserva = reservaDAO.searchModel(detachedCriteriaReserva);
		DetachedCriteria detachedCriteriaLivro = DetachedCriteria.forClass(Livro.class)
				.add(Restrictions.idEq(reserva.getLivro().getId()));
		Livro livro = livroDAO.searchModel(detachedCriteriaLivro);
		livro.setQuantidade(livro.getQuantidade() + reserva.getQuantidade());
		livroDAO.update(livro);
		reservaDAO.delete(reserva);
		ModelAndView mv = new ModelAndView("reserva/confirma-reserva");
		return mv;
	}

}
