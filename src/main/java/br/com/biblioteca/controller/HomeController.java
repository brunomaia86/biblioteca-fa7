package br.com.biblioteca.controller;

import javax.servlet.http.HttpSession;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.biblioteca.dao.PessoaDAO;
import br.com.biblioteca.model.Pessoa;

@Controller
public class HomeController {

	@Autowired
	private PessoaDAO pessoaDAO;
	
	@RequestMapping("/inicio")
	public String index() {
		return "index";
	}
	
	@RequestMapping("/login")
	public String loginForm(){
		
		return "login";
	}
	
	@RequestMapping("/formCadastro")
	public String formCadastro(){
		
		return "novo-cadastro";
	}
	
	@RequestMapping(value = "/novoCadastro", method = RequestMethod.GET)
	public String cadastro(Pessoa pessoa) {
		pessoaDAO.save(pessoa);
		return "login";
	}

	@RequestMapping("/efetuaLogin")
	public String efetuarLogin(Pessoa pessoa, HttpSession sessao, HttpSession usuariologado) {
		
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Pessoa.class)
				.add(Restrictions.ilike("login", pessoa.getlogin()))
				.add(Restrictions.ilike("senha", pessoa.getSenha()));
		
		if (pessoa.equals(pessoaDAO.searchModel(detachedCriteria))) {
			Pessoa pessoaLogada = pessoaDAO.searchModel(detachedCriteria);
			usuariologado.setAttribute("usuLogado", pessoaLogada);
			sessao.setAttribute("pessoaLogada", pessoa);
			return "index";
		}

		return "redirect:login";
	}
	
	@RequestMapping("/logout")
	public String logout(HttpSession session){
		session.invalidate();
		return "redirect:login";
	}

}
