package br.com.biblioteca.controller;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.biblioteca.dao.LivroDAO;
import br.com.biblioteca.model.Livro;

@Controller
@RequestMapping("/livro")
public class LivroController {
	
	@Autowired
	private LivroDAO livroDAO;

	@RequestMapping("/form")
	public String form() {
		
		return "livro/cadastro-livro";
	}
	
	@RequestMapping(value = "/cadastrar", method = RequestMethod.POST)
	public ModelAndView cadastrarLivro(@ModelAttribute Livro livro) {
		livroDAO.save(livro);
		ModelAndView model = new ModelAndView("livro/cadastro-sucesso");
		String msg = "Livro cadastrado com sucesso!";
		model.addObject("msg", msg);
		return model;
	}
	
	@RequestMapping("/listar")
	public ModelAndView listarLivro() {
		List<Livro> livros = livroDAO.searchModels(DetachedCriteria.forClass(Livro.class));
		ModelAndView model = new ModelAndView("livro/lista-livros");
		model.addObject("livros", livros);
		System.out.println("passou!!!");
		return model;
	}
	
	@RequestMapping("/mostrar")
	public ModelAndView mostrarLivro(int id) {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Livro.class).add(Restrictions.idEq(id));
		Livro livro = livroDAO.searchModel(detachedCriteria);
		ModelAndView mv = new ModelAndView("livro/altera-livro");
		mv.addObject("livro", livro);
		return mv;
	}
	
	@RequestMapping("/alterar")
	public ModelAndView alterarLivro (Livro livro) {
		Livro livroAlterado = livroDAO.update(livro);
		ModelAndView mv = new ModelAndView("livro/cadastro-sucesso");
		mv.addObject("livroAlterado", livroAlterado);
		return mv;
	}
}
