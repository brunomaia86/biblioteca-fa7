package br.com.biblioteca.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.criterion.DetachedCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.biblioteca.dao.SugestaoDAO;
import br.com.biblioteca.model.Livro;
import br.com.biblioteca.model.Pessoa;
import br.com.biblioteca.model.Sugestao;

@Controller
@RequestMapping("/sugestao")
public class SugestaoController {
	
	@Autowired
	private SugestaoDAO sugestaoDAO;

	@RequestMapping("/form") 
	public String form() {
		return "sugestao/cadastro-sugestao";
	}
	
	@RequestMapping(value="/sugerirNovosLivros", method = RequestMethod.POST )
	public ModelAndView sugerirLivro(@ModelAttribute Sugestao sugestao, HttpSession request) {
		sugestao.setPessoa((Pessoa) request.getAttribute("usuLogado"));
		System.out.println(sugestao.getPessoa().getNome());
		sugestaoDAO.save(sugestao);
		ModelAndView mv = new ModelAndView("sugestao/sugestao-cadastrada");
		mv.addObject("sugestoes", sugestaoDAO.searchModels(DetachedCriteria.forClass(Sugestao.class)));
		return mv;
	}
	
}
