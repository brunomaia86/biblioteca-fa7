package br.com.biblioteca.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import br.com.biblioteca.model.Livro;

public class LivroDAO extends GenericDAO<Livro> {

	@Override
	public Livro save(Livro entity) {
		// TODO Auto-generated method stub
		return super.save(entity);
	}

	@Override
	public Livro update(Livro entity) {
		// TODO Auto-generated method stub
		return super.update(entity);
	}

	@Override
	public void delete(Livro entity) {
		// TODO Auto-generated method stub
		super.delete(entity);
	}

	@Override
	public Livro searchModel(DetachedCriteria query) {
		// TODO Auto-generated method stub
		return super.searchModel(query);
	}

	@Override
	public List<Livro> searchModels(DetachedCriteria query) {
		// TODO Auto-generated method stub
		return super.searchModels(query);
	}

}
