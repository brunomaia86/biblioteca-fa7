package br.com.biblioteca.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import br.com.biblioteca.model.Sugestao;

public class SugestaoDAO extends GenericDAO<Sugestao> {

	@Override
	public Sugestao save(Sugestao entity) {
		// TODO Auto-generated method stub
		return super.save(entity);
	}
	
	@Override
	public Sugestao update(Sugestao entity) {
		// TODO Auto-generated method stub
		return super.update(entity);
	}
	
	@Override
	public void delete(Sugestao entity) {
		// TODO Auto-generated method stub
		super.delete(entity);
	}
	
	@Override
	public Sugestao searchModel(DetachedCriteria query) {
		// TODO Auto-generated method stub
		return super.searchModel(query);
	}
	
	@Override
	public List<Sugestao> searchModels(DetachedCriteria query) {
		// TODO Auto-generated method stub
		return super.searchModels(query);
	}
}
