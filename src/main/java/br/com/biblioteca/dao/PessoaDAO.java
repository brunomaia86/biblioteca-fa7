package br.com.biblioteca.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import br.com.biblioteca.model.Pessoa;

public class PessoaDAO extends GenericDAO<Pessoa> {

	@Override
	public Pessoa save(Pessoa entity) {
		// TODO Auto-generated method stub
		return super.save(entity);
	}

	@Override
	public Pessoa update(Pessoa entity) {
		// TODO Auto-generated method stub
		return super.update(entity);
	}
	
	@Override
	public void delete(Pessoa entity) {
		// TODO Auto-generated method stub
		super.delete(entity);
	}
	
	@Override
	public Pessoa searchModel(DetachedCriteria query) {
		// TODO Auto-generated method stub
		return super.searchModel(query);
	}
	
	@Override
	public List<Pessoa> searchModels(DetachedCriteria query) {
		// TODO Auto-generated method stub
		return super.searchModels(query);
	}
	
	
}
