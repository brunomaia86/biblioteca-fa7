package br.com.biblioteca.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import br.com.biblioteca.model.ReservaSimples;

public class ReservaDAO extends GenericDAO<ReservaSimples> {

	@Override
	public ReservaSimples save(ReservaSimples entity) {
		// TODO Auto-generated method stub
		return super.save(entity);
	}

	@Override
	public void delete(ReservaSimples entity) {
		// TODO Auto-generated method stub
		super.delete(entity);
	}

	@Override
	public ReservaSimples update(ReservaSimples entity) {
		// TODO Auto-generated method stub
		return super.update(entity);
	}

	@Override
	public ReservaSimples searchModel(DetachedCriteria query) {
		// TODO Auto-generated method stub
		return super.searchModel(query);
	}

	@Override
	public List<ReservaSimples> searchModels(DetachedCriteria query) {
		// TODO Auto-generated method stub
		return super.searchModels(query);
	}

}
