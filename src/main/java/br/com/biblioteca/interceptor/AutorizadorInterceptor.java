package br.com.biblioteca.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class AutorizadorInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object controller)
			throws Exception {

		String uri = request.getRequestURI();

		if (uri.endsWith("login") || 
					uri.endsWith("novoCadastro") ||
						uri.endsWith("formCadastro") ||
							uri.endsWith("efetuaLogin"))
			// uri.contains("assets"))
			return true;

		if (request.getSession().getAttribute("pessoaLogada") != null)
			return true;

		response.sendRedirect("/biblioteca/login");
		return false;
	}

}
