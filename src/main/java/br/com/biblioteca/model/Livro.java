package br.com.biblioteca.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "livro")
public class Livro {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idlivro")
	private int id;
	private String nome;
	private String escritor;
	private int anoedicao;
	private int classificacao;
	private int quantidade;

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Livro) {
			// Algoritmo que verifica se sao iguais
			return true;
		}
		return false;
	}

	public String getIdlivroString() {
		return String.valueOf(id);
	}

	public int getAnoedicao() {
		return anoedicao;
	}

	public void setAnoedicao(int anoedicao) {
		this.anoedicao = anoedicao;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEscritor() {
		return escritor;
	}

	public void setEscritor(String escritor) {
		this.escritor = escritor;
	}

	public int getClassificacao() {
		return classificacao;
	}

	public void setClassificacao(int classificacao) {
		this.classificacao = classificacao;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

}