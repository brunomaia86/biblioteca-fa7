package br.com.biblioteca.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "reserva")
public class ReservaSimples {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idreserva")
	private int idReserva;

	@ManyToOne(optional = false, targetEntity = Pessoa.class)
	@JoinColumn(name = "idpessoa")
	private Pessoa pessoa;

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "idlivro")
	private Livro livro;
	private int quantidade;
	
	

	public ReservaSimples() {
		super();
	}

	public ReservaSimples(Pessoa pessoa, Livro livro, int quantidade) {
		super();
		this.pessoa = pessoa;
		this.livro = livro;
		this.quantidade = quantidade;
	}

	
	
	public int getIdReserva() {
		return idReserva;
	}

	public void setIdReserva(int idReserva) {
		this.idReserva = idReserva;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Livro getLivro() {
		return livro;
	}

	public void setLivro(Livro livro) {
		this.livro = livro;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quntidade) {
		this.quantidade = quntidade;
	}

}