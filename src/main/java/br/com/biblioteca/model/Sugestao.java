package br.com.biblioteca.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sugestao")
public class Sugestao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idsugestao")
	private int id;

	@ManyToOne(optional = false, targetEntity = Pessoa.class)
	@JoinColumn(name = "idpessoa")
	private Pessoa pessoa;
	private String titulo;
	private String autor;

	public Sugestao() {
		super();
	}

	public Sugestao(Pessoa pessoa, String titulo, String autor) {
		super();
		this.pessoa = pessoa;
		this.titulo = titulo;
		this.autor = autor;
	}
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

}
